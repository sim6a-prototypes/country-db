APP_NAME=country-db-api
BUILD_DIR=build/package

.PHONY: all
all: help

## build: Compile the app.
.PHONY: build
build:
	GO111MODULE=on \
    CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64 \
	go build -tags=jsoniter -o ${BUILD_DIR}/${APP_NAME} cmd/restapi/main.go

## clean: Delete binary file of the app.
.PHONY: clean
clean:
	rm ${BUILD_DIR}/${APP_NAME}

## build-docker-img: Build a docker image of the app.
.PHONY: build-docker-img
build-docker-img: build
	docker build . -f ${BUILD_DIR}/Dockerfile -t ${APP_NAME}

## run-in-docker: Run the app and infrastructure in docker on local machine.
.PHONY: run-in-docker
run-in-docker: build
	docker-compose -f ./deployments/docker-compose.local.yml -p local up -d --build \
		--scale country-db-api=2

## stop-in-docker: Stop all docker containers those serves app on local machine.
.PHONY: stop-in-docker
stop-in-docker:
	docker-compose -f ./deployments/docker-compose.local.yml -p local down \
		--remove-orphans

## help: Print prompt.
.PHONY: help
help: Makefile
	@echo
	@echo "Choose a command to run:"
	@echo
	@sed -n 's/^##//p' $< | column -t -s ':' |  sed -e 's/^/ /'
	@echo
