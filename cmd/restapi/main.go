package main

import "country-db/pkg/system/startup"

func main() {
	startup.RunRestAPI()
}
