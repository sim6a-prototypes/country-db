#!/bin/bash

generate_random_string() {
    head /dev/urandom | tr -dc A-Za-z0-9 | head -c 13 ; echo ''
}
