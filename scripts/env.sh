#!/bin/bash

export_postgres_env() {
    local POSTGRES_ENV_FILE=./deployments/env/postgres.local.env
    export $(egrep -v '^#' ${POSTGRES_ENV_FILE} | xargs)
    export POSTGRES_PORT=$(get_postgres_port_in_host_network)
}

get_postgres_port_in_host_network() {
    COMPOSE_FILE=./deployments/docker-compose.local.yml
    yq r ${COMPOSE_FILE} services.postgres.ports | grep -o -P '(?<=").*(?=:5432)'
}