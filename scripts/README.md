# Scripts

Run all scripts from project root directory. See scripts to find arguments info.

## Postgres migrator

Use the migrate utility `https://github.com/golang-migrate/migrate/tree/master/cmd/migrate`. Binary file *migrate* must be included the *PATH* environment variable. You can use development tools installer to install it.

| Script                                      | Descripton                        |
|---------------------------------------------|-----------------------------------|
| ./scripts/pg-migrator.sh new migration_name | Create a new migration with name *migration_name*. |
| ./scripts/pg-migrator.sh up [N]             | Apply all or N up migrations.     |
| ./scripts/pg-migrator.sh down [N]           | Apply all or N down migrations.   |
| ./scripts/pg-migrator.sh drop               | Drop everything inside database.  |

## Jq

Jq is a lightweight and flexible command-line JSON processor used for work with application config in bash. Binary file *jq* must be included the *PATH* environment variable. You can install it from `https://stedolan.github.io/jq/download/` or use development tools installer. 

## Configuration manager

Configuration manager will help you to work with application configuration files.

| Script                          | Descripton                        |
|---------------------------------|-----------------------------------|
| ./scripts/configuration.sh new  | Create a new configuration to start working with application. |
| ./scripts/configuration.sh sync | Update the application configuration if you chang .env files. |

## Development tools installer

Run `./scripts/install-dev-tools.sh` under the root user (or via *sudo*) to install all development tools automatically. 