#!/bin/bash

echo "Add migrate repository ..."
curl -L https://packagecloud.io/golang-migrate/migrate/gpgkey | apt-key add -
echo "deb https://packagecloud.io/golang-migrate/migrate/ubuntu/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/migrate.list

echo "Add yq repository ..."
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys CC86BB64
add-apt-repository ppa:rmescandon/yq -y

echo "Update packet index ..."
apt-get update

echo "Install packets ..."
apt-get install -y migrate
apt-get install -y jq
apt install yq -y

echo "Check the installaction ..."

echo "Migrate version:"
migrate --version

echo "jq version:"
jq --version

echo "yq version:"
yq --version