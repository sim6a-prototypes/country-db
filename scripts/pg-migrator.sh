#!/bin/bash

# import export_postgres_env
. ./scripts/env.sh --source-only

export_postgres_env

POSTGRES_URL=postgres://${POSTGRES_USER}:${POSTGRES_PASSWORD}@localhost:${POSTGRES_PORT}/${POSTGRES_DB}?sslmode=disable

MIGRATIONS_DIR=./pkg/outports/postgres/migrations
MIGRATE_COMMAND="migrate -path ${MIGRATIONS_DIR} -database ${POSTGRES_URL}"

create_migration() {
    local MIGRATION_FILE_NAME=${1}
    migrate create -ext sql -dir ${MIGRATIONS_DIR} -seq ${MIGRATION_FILE_NAME}
}

apply_up_migrations() {
    local MIGRATIONS_QUANTITY=${1}
    ${MIGRATE_COMMAND} up ${MIGRATIONS_QUANTITY}
}

apply_down_migrations() {
    local MIGRATIONS_QUANTITY=${1}
    ${MIGRATE_COMMAND} down ${MIGRATIONS_QUANTITY}
}

drop_everything_inside_database() {
    ${MIGRATE_COMMAND} drop
}

COMMAND=${1}
case ${COMMAND} in
    new)
        create_migration ${2}
        ;;
    up)
        apply_up_migrations ${2}
        ;;
    down)
        apply_down_migrations ${2}
        ;;
    drop)
        drop_everything_inside_database
        ;;
esac





