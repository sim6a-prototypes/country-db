#!/bin/bash

ZOOKEEPER_OPTION="--zookeeper localhost:2181"
BROKER_LIST_OPTION="--broker-list localhost:9092"
BOOTSTRAP_SERVER_OPTION="--bootstrap-server localhost:9092"

create_topic() {
    local TOPIC_NAME=${1}
    kafka-topics.sh --create ${ZOOKEEPER_OPTION} \
        --replication-factor 1 --partitions 1 --topic ${TOPIC_NAME}
}

delete_topic() {
    local TOPIC_NAME=${1}
    kafka-topics.sh --delete ${ZOOKEEPER_OPTION} --topic ${TOPIC_NAME}
}

list_topics() {
    kafka-topics.sh --list ${ZOOKEEPER_OPTION}
}

run_producer() {
    local TOPIC_NAME=${1}
    kafka-console-producer.sh ${BROKER_LIST_OPTION} --topic ${TOPIC_NAME}
}

run_consumer() {
    local TOPIC_NAME=${1}
    kafka-console-consumer.sh ${BOOTSTRAP_SERVER_OPTION} --topic ${TOPIC_NAME} --from-beginning
}

COMMAND=${1}
case ${COMMAND} in
    topic)
        create_topic ${2}
        ;;
    list)
        list_topics
        ;;
    delete)
        delete_topic ${2}
        ;;
    producer)
        run_producer ${2}
        ;;
    consumer)
        run_consumer ${2}
        ;;
esac