#!/bin/bash

# import generate_random_string
. ./scripts/random.sh --source-only

# import export_postgres_env
. ./scripts/env.sh --source-only

CONFIG_FILE=./build/package/etc/config.json
SAMPLE_CONFIG_FILE=${CONFIG_FILE}.sample
POSTGRES_ENV_FILE=./deployments/env/postgres.local.env
SAMPLE_POSTGRES_ENV_FILE=${POSTGRES_ENV_FILE}.sample


insert_postgres_connection_data_in_app_config() {
    local TMP_CONFIG_FILE=/tmp/$(generate_random_string).country-db-api.tmp

    export_postgres_env

    cat ${CONFIG_FILE} | jq ".postgres.user=env.POSTGRES_USER" > ${TMP_CONFIG_FILE}
    cat ${TMP_CONFIG_FILE} > ${CONFIG_FILE}

    cat ${CONFIG_FILE} | jq ".postgres.password=env.POSTGRES_PASSWORD" > ${TMP_CONFIG_FILE}
    cat ${TMP_CONFIG_FILE} > ${CONFIG_FILE}

    cat ${CONFIG_FILE} | jq ".postgres.dbName=env.POSTGRES_DB" > ${TMP_CONFIG_FILE}
    cat ${TMP_CONFIG_FILE} > ${CONFIG_FILE}

    echo "Postgres connection data added to the application config."
}

create_default_config() {
    cp ${SAMPLE_POSTGRES_ENV_FILE} ${POSTGRES_ENV_FILE}
    echo "Default postgres .env file created by sample."
    echo "See ${POSTGRES_ENV_FILE}"

    cp ${SAMPLE_CONFIG_FILE} ${CONFIG_FILE}
    echo "Default application config created by sample."
    echo "See ${CONFIG_FILE}"

    insert_postgres_connection_data_in_app_config
}

COMMAND=${1}
case ${COMMAND} in
    new)
        create_default_config
        ;;
    sync)
        insert_postgres_connection_data_in_app_config
        ;;
esac
