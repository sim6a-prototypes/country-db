module country-db

go 1.15

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.0.0
	google.golang.org/appengine v1.6.7 // indirect
)
