package stdlogger

import (
	"country-db/pkg/features/country"
	"country-db/pkg/system"
	"country-db/pkg/system/errors"
	"fmt"
)

func New(systemTime system.Time) *Logger {
	l := Logger{
		systemTime: systemTime,
	}
	l.initEventHandlers()

	return &l
}

type Logger struct {
	systemTime system.Time

	eventHanlders map[string]func(event system.Event)
}

func (l *Logger) initEventHandlers() {
	l.eventHanlders = map[string]func(event system.Event){
		country.EventCacheHit{}.Type():             l.logCountryEventCacheHit,
		country.EventCacheMiss{}.Type():            l.logCountryEventCacheMiss,
		country.EventCached{}.Type():               l.logCountryEventCached,
		country.EventNotCached{}.Type():            l.logCountryEventNotCached,
		country.EventReserveRepositoryHit{}.Type(): l.logCountryEventReserveRepositoryHit,
		errors.EventUnexpected{}.Type():            l.logErrorUnexpected,
	}
}

func (l *Logger) Handle(event system.Event) {
	if handle, ok := l.eventHanlders[event.Type()]; ok {
		handle(event)
	}
}

func (l *Logger) LogInfo(msg string) {
	fmt.Println("INFO", l.systemTime.Now(), msg)
}

func (l *Logger) LogWarn(msg string) {
	fmt.Println("WARN", l.systemTime.Now(), msg)
}

func (l *Logger) LogError(err error) {
	fmt.Println("ERROR", l.systemTime.Now(), err.Error())
}
