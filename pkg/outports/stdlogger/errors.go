package stdlogger

import (
	"country-db/pkg/system"
	"country-db/pkg/system/errors"
)

func (l *Logger) logErrorUnexpected(event system.Event) {
	e := event.(*errors.EventUnexpected)

	l.LogError(e.Detail)
}
