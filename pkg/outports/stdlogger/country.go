package stdlogger

import (
	"country-db/pkg/features/country"
	"country-db/pkg/system"
	"fmt"
)

func (l *Logger) logCountryEventCacheMiss(event system.Event) {
	e := event.(*country.EventCacheMiss)

	l.LogInfo(fmt.Sprintf(
		"The code %s absent in the local cache. Trying to find info in the reserve source.",
		e.Code.String()),
	)
}

func (l *Logger) logCountryEventCacheHit(event system.Event) {
	e := event.(*country.EventCacheHit)

	l.LogInfo(fmt.Sprintf(
		"The code %s is presented in the local cache.",
		e.Code.String()),
	)
}

func (l *Logger) logCountryEventReserveRepositoryHit(event system.Event) {
	e := event.(*country.EventReserveRepositoryHit)

	l.LogInfo(fmt.Sprintf(
		"Info about a country with code %s found in the reserve source.",
		e.Code.String()),
	)
}

func (l *Logger) logCountryEventCached(event system.Event) {
	e := event.(*country.EventCached)

	l.LogInfo(fmt.Sprintf(
		"Info about a country with code %s added in the local cahce.",
		e.Code.String()),
	)
}

func (l *Logger) logCountryEventNotCached(event system.Event) {
	e := event.(*country.EventNotCached)

	l.LogWarn(fmt.Sprintf(
		"Can't add info about a country with code %s in local cache: %s",
		e.Code.String(), e.Err.Error()),
	)
}
