package restcountries

import (
	"country-db/pkg/features/country"
	"country-db/pkg/system"
	"country-db/pkg/system/errors"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type Adapter struct {
	HTTPClient system.HTTPClient

	CountryByCodeURL string
	Host             string
	Key              string
}

func (a *Adapter) Get(code country.Alpha2Or3Code) (countryEntity country.Country, err error) {
	req, _ := http.NewRequest(http.MethodGet, a.CountryByCodeURL+code.String(), nil)
	req.Header.Add("x-rapidapi-host", a.Host)
	req.Header.Add("x-rapidapi-key", a.Key)

	res, err := a.HTTPClient.Do(req)
	if err != nil {
		return country.Country{}, err
	}
	defer func() {
		if cerr := res.Body.Close(); cerr != nil {
			if err != nil {
				err = fmt.Errorf("%s: %w", err.Error(), cerr)
			} else {
				err = cerr
			}
		}
	}()

	if res.StatusCode == http.StatusNotFound {
		return country.Country{}, errors.NewNotFoundError(country.ErrNotFound)
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return country.Country{}, err
	}

	countryEntity = country.Country{}

	if err = json.Unmarshal(body, &countryEntity); err != nil {
		return country.Country{}, err
	}

	return countryEntity, nil
}
