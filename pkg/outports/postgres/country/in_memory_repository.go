package country

import (
	"country-db/pkg/system/errors"
	"country-db/pkg/features/country"
)

type InMemoryRepository struct {
	countries []country.Country
}

func (r *InMemoryRepository) Add(country country.Country) error {
	r.countries = append(r.countries, country)
	return nil
}

func (r *InMemoryRepository) Get(code country.Alpha2Or3Code) (country.Country, error) {
	for _, c := range r.countries {
		if c.HasCode(code) {
			return c, nil
		}
	}

	return country.Country{}, errors.NewNotFoundError(country.ErrNotFound)
}
