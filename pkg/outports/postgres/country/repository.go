package country

import (
	"country-db/pkg/features/country"
	"country-db/pkg/system/errors"
	"database/sql"

	"github.com/jmoiron/sqlx"
)

type Repository struct {
	Connection *sqlx.DB
}

func (r *Repository) Add(country country.Country) error {
	_, err := r.Connection.NamedExec(`
		INSERT INTO countries (name, alpha_2_code, alpha_3_code, capital) 
		VALUES (:name, :alpha_2_code, :alpha_3_code, :capital)
	`, &country)

	return err
}

func (r *Repository) Get(code country.Alpha2Or3Code) (country.Country, error) {
	c := country.Country{}

	err := r.Connection.Get(&c, `
		SELECT name, alpha_2_code, alpha_3_code, capital 
		FROM countries 
		WHERE alpha_2_code=$1 OR alpha_3_code=$1
	`, code.String())
	if err == sql.ErrNoRows {
		return country.Country{}, errors.NewNotFoundError(country.ErrNotFound)
	}

	return c, err
}
