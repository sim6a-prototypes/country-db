BEGIN;

CREATE TABLE countries (
    id            SERIAL CONSTRAINT pk_countries PRIMARY KEY,
    name          varchar(64) NOT NULL,
    alpha_2_code  char(2) NOT NULL,
    alpha_3_code  char(3) NOT NULL,
    capital       varchar(64) NOT NULL
);

COMMIT;