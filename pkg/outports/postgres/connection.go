package postgres

import (
	"country-db/pkg/system"
	"country-db/pkg/system/logger"
	"fmt"

	"github.com/jmoiron/sqlx"

	// Postgres driver
	_ "github.com/lib/pq"
)

var (
	connection *sqlx.DB
)

func GetConnection() *sqlx.DB {
	return connection
}

type Connector struct {
	Logger logger.Logger
}

func (c *Connector) Connect(host, user, password, dbName string) (system.CleanupFunc, error) {
	db, err := sqlx.Connect("postgres",
		fmt.Sprintf("host=%s user=%s password=%s dbname=%s sslmode=disable",
			host, user, password, dbName))
	if err != nil {
		return nil, err
	}

	connection = db
	return func() {
		if err := connection.Close(); err != nil {
			c.Logger.LogError(err)
			return
		}
		c.Logger.LogInfo("A postgres connection closed.")
	}, nil
}
