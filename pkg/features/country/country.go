package country

import "strings"

type Alpha2Or3Code string

func (code Alpha2Or3Code) String() string {
	return strings.ToUpper(string(code))
}

type Country struct {
	Name       string `json:"name" db:"name"`
	Alpha2Code string `json:"alpha2Code" db:"alpha_2_code"`
	Alpha3Code string `json:"alpha3Code" db:"alpha_3_code"`
	Capital    string `json:"capital" db:"capital"`
}

func (c Country) HasCode(code Alpha2Or3Code) bool {
	codeString := code.String()
	return c.Alpha2Code == codeString || c.Alpha3Code == codeString
}
