package country

import (
	"country-db/pkg/inports/restapi/http"
	"country-db/pkg/system"

	"github.com/gin-gonic/gin"
)

func NewHTTPHandler(
	countryRepository repository,
	reserveCountryRepository reserveRepository,
	eventBus system.EventBus,
) HTTPHandler {
	return HTTPHandler{
		countryCache: cache{
			countryRepository:        countryRepository,
			reserveCountryRepository: reserveCountryRepository,
			validator:                validator{},
			eventBus:                 eventBus,
		},
	}
}

type HTTPHandler struct {
	countryCache cache
}

func (h HTTPHandler) GetCountryByCode(ctx *gin.Context) {
	code := http.GetCodeParam(ctx)
	countryEntity, err := h.countryCache.GetCountryByCode(Alpha2Or3Code(code))
	if err != nil {
		_ = ctx.Error(err)
		return
	}

	http.RespondWithOK(ctx, &countryEntity)
}
