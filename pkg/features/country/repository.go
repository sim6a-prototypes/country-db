package country

type repository interface {
	Add(country Country) error
	Get(code Alpha2Or3Code) (Country, error)
}
