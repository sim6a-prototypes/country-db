package country

import "errors"

var (
	ErrNotFound = errors.New("Country not found.")
)
