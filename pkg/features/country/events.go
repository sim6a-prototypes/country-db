package country

type EventCacheMiss struct {
	Code Alpha2Or3Code
}

func (e EventCacheMiss) Type() string {
	return "country_cache_miss"
}

type EventCacheHit struct {
	Code Alpha2Or3Code
}

func (e EventCacheHit) Type() string {
	return "country_cache_hit"
}

type EventReserveRepositoryHit struct {
	Code Alpha2Or3Code
}

func (e EventReserveRepositoryHit) Type() string {
	return "country_reserve_repository_hit"
}

type EventCached struct {
	Code Alpha2Or3Code
}

func (e EventCached) Type() string {
	return "country_cached"
}

type EventNotCached struct {
	Err  error
	Code Alpha2Or3Code
}

func (e EventNotCached) Type() string {
	return "country_not_cached"
}
