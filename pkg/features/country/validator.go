package country

import (
	"country-db/pkg/system/errors"
	stderrors "errors"
)

var (
	errInvalidAlpha2Or3Code = stderrors.New("Only alpha2 or alpha3 codes are supported.")
)

const (
	symbolsInAlpha2Code = 2
	symbolsInAlpha3Code = 3
)

type validator struct {
}

func (v validator) validateCode(code Alpha2Or3Code) error {
	if len(code) < symbolsInAlpha2Code || len(code) > symbolsInAlpha3Code {
		return errors.NewValidationError(errInvalidAlpha2Or3Code)
	}

	return nil
}
