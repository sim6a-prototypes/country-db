package country

import (
	"country-db/pkg/system"
	"country-db/pkg/system/errors"
)

type reserveRepository interface {
	Get(code Alpha2Or3Code) (Country, error)
}

type cache struct {
	countryRepository        repository
	reserveCountryRepository reserveRepository
	validator                validator
	eventBus                 system.EventBus
}

func (c cache) GetCountryByCode(code Alpha2Or3Code) (Country, error) {
	if err := c.validator.validateCode(code); err != nil {
		return Country{}, err
	}

	countryEntity, err := c.countryRepository.Get(code)
	if err == nil {
		c.eventBus.Publish(&EventCacheHit{Code: code})
		return countryEntity, nil
	}

	if !errors.IsNotFound(err) {
		return Country{}, err
	}

	c.eventBus.Publish(&EventCacheMiss{Code: code})
	countryEntity, err = c.reserveCountryRepository.Get(code)
	if err != nil {
		return Country{}, err
	}

	c.eventBus.Publish(&EventReserveRepositoryHit{Code: code})
	if err = c.countryRepository.Add(countryEntity); err != nil {
		c.eventBus.Publish(&EventNotCached{Code: code, Err: err})
	} else {
		c.eventBus.Publish(&EventCached{Code: code})
	}

	return countryEntity, nil
}
