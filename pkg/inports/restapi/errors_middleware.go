package restapi

import (
	"country-db/pkg/inports/restapi/http"
	"country-db/pkg/system"
	"country-db/pkg/system/errors"
	stderrors "errors"
	stdhttp "net/http"

	"github.com/gin-gonic/gin"
)

func ErrorHandlingMiddleware(eventBus system.EventBus) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ctx.Next()

		if len(ctx.Errors) == 0 {
			return
		}

		err := ctx.Errors[0].Err
		var appError *errors.Error

		if stderrors.As(err, &appError) {
			if appError.HTTPStatus == stdhttp.StatusNotFound {
				ctx.AbortWithStatus(stdhttp.StatusNotFound)
				return
			}

			http.RespondWithStatus(ctx, appError.HTTPStatus, appError)
			return
		}

		unexpectedError := errors.NewUnexpectedError(err)
		eventBus.Publish(&errors.EventUnexpected{
			Detail: unexpectedError,
		})
		serviceUnavailableError := errors.NewServiceUnavailableError()
		http.RespondWithStatus(ctx, serviceUnavailableError.HTTPStatus, serviceUnavailableError)
	}
}
