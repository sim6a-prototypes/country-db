package restapi

import (
	"context"
	"country-db/pkg/system/logger"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

type HTTPServer struct {
	Router http.Handler
	Logger logger.Logger

	Address            string
	ShutdownTimeoutSec int
}

func (s HTTPServer) Run() {
	server := &http.Server{
		Addr:    s.Address,
		Handler: s.Router,
	}

	// Initializing the server in a goroutine so that
	// it won't block the graceful shutdown handling below
	go func() {
		err := server.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			s.Logger.LogError(err)
		}
	}()

	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout.
	quit := make(chan os.Signal)
	// kill (no param) default send syscall.SIGTERM
	// kill -2 is syscall.SIGINT
	// kill -9 is syscall.SIGKILL but can't be catch, so don't need add it
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	s.Logger.LogInfo("Http server is ready to accept requests.")
	<-quit
	s.Logger.LogInfo("Http server is stopping.")

	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(s.ShutdownTimeoutSec)*time.Second)
	defer cancel()
	if err := server.Shutdown(ctx); err != nil {
		s.Logger.LogError(err)
	}

	s.Logger.LogInfo("Http server is stopped.")
}
