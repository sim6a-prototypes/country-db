package restapi

import (
	"country-db/pkg/features/country"

	"github.com/gin-gonic/gin"
)

type RouterFactory struct {
	CountryHandler          country.HTTPHandler
	ErrorHandlingMiddleware gin.HandlerFunc
}

func (f RouterFactory) NewRouter() *gin.Engine {
	gin.SetMode(gin.DebugMode)
	router := gin.New()

	router.Use(f.ErrorHandlingMiddleware)

	router.GET("/countries/:code", f.CountryHandler.GetCountryByCode)

	return router
}
