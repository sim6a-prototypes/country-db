package http

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func RespondWithOK(ctx *gin.Context, obj interface{}) {
	RespondWithStatus(ctx, http.StatusOK, obj)
}

func RespondWithStatus(ctx *gin.Context, status int, obj interface{}) {
	ctx.JSON(status, obj)
}
