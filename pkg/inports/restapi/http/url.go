package http

import "github.com/gin-gonic/gin"

const (
	codeParam = "code"
)

func GetCodeParam(ctx *gin.Context) string {
	return ctx.Param(codeParam)
}