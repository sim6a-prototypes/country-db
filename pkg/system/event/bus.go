package event

import "country-db/pkg/system"

type handler interface {
	Handle(event system.Event)
}

type Bus struct {
	handlers []handler
}

func (b *Bus) Connect(h handler) {
	b.handlers = append(b.handlers, h)
}

func (b *Bus) Publish(event system.Event) {
	for _, h := range b.handlers {
		h.Handle(event)
	}
}
