package logger

import (
	"fmt"
)

type Logger interface {
	LogInfo(msg string)
	LogWarn(msg string)
	LogError(err error)
}

// LogDebug is used to print temporary debug messages
// on the developer machine. Also can be used to print
// messages when the main logger isn't ready.
func LogDebug(msg string) {
	fmt.Println("DEBUG", msg)
}