package time

import "time"

type Time struct {
}

func (c *Time) Now() time.Time {
	return time.Now()
}
