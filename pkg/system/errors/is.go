package errors

import "errors"

func IsNotFound(err error) bool {
	var e *Error
	if errors.As(err, &e) {
		if e.Type == notFoundErrorType {
			return true
		}
	}

	return false
}
