package errors

type EventUnexpected struct {
	Detail error
}

func (e EventUnexpected) Type() string {
	return "errors_unexpected"
}
