package errors

import (
	"fmt"
	"net/http"
)

const (
	notFoundErrorType = "NOT_FOUND"
)

type Error struct {
	Type       string   `json:"type"`
	Title      string   `json:"title"`
	Detail     string   `json:"detail,omitempty"`
	Details    []string `json:"details,omitempty"`
	HTTPStatus int      `json:"status"`
}

func (e *Error) Error() string {
	return fmt.Sprintf("[%s] %s", e.Type, e.Detail)
}

func (e *Error) AddDetail(err error) {
	if len(e.Details) > 0 {
		e.Details = append(e.Details, err.Error())
		return
	}

	if e.Detail == "" {
		e.Detail = err.Error()
		return
	}

	e.Details = []string{
		e.Detail,
		err.Error(),
	}
	e.Detail = ""
}

func NewNotFoundError(err error) *Error {
	return &Error{
		Type:   notFoundErrorType,
		Detail: err.Error(),
	}
}

func NewValidationError(err error) *Error {
	return &Error{
		Type:       "VALIDATION",
		Title:      "User input is invalid.",
		Detail:     err.Error(),
		HTTPStatus: http.StatusBadRequest,
	}
}

func NewUnexpectedError(err error) *Error {
	return &Error{
		Type:   "UNEXPECTED",
		Detail: err.Error(),
	}
}

func NewServiceUnavailableError() *Error {
	return &Error{
		Type:       "SERVICE_UNAVAILABLE",
		Title:      "Service is unavailable.",
		HTTPStatus: http.StatusInternalServerError,
	}
}
