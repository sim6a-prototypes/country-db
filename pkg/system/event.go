package system

type Event interface {
	Type() string
}

type EventBus interface {
	Publish(event Event)
}
