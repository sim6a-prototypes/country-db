package startup

import (
	"country-db/pkg/features/country"
	"country-db/pkg/inports/restapi"
	"country-db/pkg/outports/postgres"
	countrydb "country-db/pkg/outports/postgres/country"
	"country-db/pkg/outports/rapidapi/restcountries"
	"country-db/pkg/outports/stdlogger"
	"country-db/pkg/system/config"
	"country-db/pkg/system/event"
	systime "country-db/pkg/system/time"
	"net/http"

	"github.com/gin-gonic/gin"
)

type Container struct {
	clock    *systime.Time
	logger   *stdlogger.Logger
	eventBus *event.Bus
}

func (c Container) getSystemTime() *systime.Time {
	if c.clock == nil {
		c.clock = &systime.Time{}
	}

	return c.clock
}

func (c Container) getLogger() *stdlogger.Logger {
	if c.logger == nil {
		c.logger = stdlogger.New(c.getSystemTime())
	}

	return c.logger
}

func (c Container) getEventBus() *event.Bus {
	if c.eventBus == nil {
		c.eventBus = &event.Bus{}
		c.eventBus.Connect(c.getLogger())
	}

	return c.eventBus
}

func (c Container) getPostgresConnector() *postgres.Connector {
	return &postgres.Connector{
		Logger: c.getLogger(),
	}
}

func (c Container) getHTTPClient() *http.Client {
	return http.DefaultClient
}

func (c Container) getCountryRepository() *countrydb.Repository {
	return &countrydb.Repository{
		Connection: postgres.GetConnection(),
	}
}

func (c Container) getRapidAPICountriesAdapter() *restcountries.Adapter {
	cfg := config.Get()

	return &restcountries.Adapter{
		HTTPClient:       c.getHTTPClient(),
		CountryByCodeURL: cfg.RapidAPI.CountryByCodeURL,
		Host:             cfg.RapidAPI.Host,
		Key:              cfg.RapidAPI.Key,
	}
}

func (c Container) getCountryHandler() country.HTTPHandler {
	return country.NewHTTPHandler(
		c.getCountryRepository(),
		c.getRapidAPICountriesAdapter(),
		c.getEventBus(),
	)
}

func (c Container) getErrorHandlingMiddleware() gin.HandlerFunc {
	return restapi.ErrorHandlingMiddleware(c.getEventBus())
}

func (c Container) getRouterFactory() restapi.RouterFactory {
	return restapi.RouterFactory{
		CountryHandler:          c.getCountryHandler(),
		ErrorHandlingMiddleware: c.getErrorHandlingMiddleware(),
	}
}

func (c Container) getRouter() *gin.Engine {
	routerFactory := c.getRouterFactory()
	return routerFactory.NewRouter()
}

func (c Container) getHTTPServer() restapi.HTTPServer {
	cfg := config.Get()

	return restapi.HTTPServer{
		Router:             c.getRouter(),
		Logger:             c.getLogger(),
		Address:            cfg.HTTPServer.Address,
		ShutdownTimeoutSec: cfg.HTTPServer.ShutdownTimeoutSec,
	}
}
