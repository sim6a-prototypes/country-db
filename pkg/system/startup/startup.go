package startup

import (
	"country-db/pkg/system/config"
	"country-db/pkg/system/logger"
)

const (
	appCantBeStarted = "App can't be started."
)

func RunRestAPI() {
	if err := config.Read(); err != nil {
		logger.LogDebug("Can't read the app configuration: " + err.Error())
		logger.LogDebug(appCantBeStarted)
		return
	}

	container := Container{}
	postgresConnector := container.getPostgresConnector()
	closePostgresConnection, err := postgresConnector.Connect(
		config.Get().Postgres.Host,
		config.Get().Postgres.User,
		config.Get().Postgres.Password,
		config.Get().Postgres.DBName,
	)
	if err != nil {
		logger.LogDebug("Can't connect to postgres: " + err.Error())
		logger.LogDebug(appCantBeStarted)
		return
	}
	defer closePostgresConnection()

	httpServer := container.getHTTPServer()

	httpServer.Run()
}
