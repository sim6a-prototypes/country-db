package config

import (
	"encoding/json"
	"io/ioutil"
	"path"
)

type Config struct {
	RapidAPI struct {
		Host             string `json:"host"`
		Key              string `json:"key"`
		CountryByCodeURL string `json:"countryByCodeUrl"`
	} `json:"rapidApi"`

	Postgres struct {
		Host     string `json:"host"`
		User     string `json:"user"`
		Password string `json:"password"`
		DBName   string `json:"dbName"`
	} `json:"postgres"`

	HTTPServer struct {
		Address            string `json:"address"`
		ShutdownTimeoutSec int    `json:"shutdownTimeoutSec"`
	} `json:"httpServer"`
}

var config Config

func Get() Config {
	return config
}

var (
	configFile = path.Join("/", "etc", "country-db", "config.json")
)

func Read() error {
	var (
		configBytes []byte
		err         error
	)

	if configBytes, err = ioutil.ReadFile(configFile); err != nil {
		return err
	}

	return json.Unmarshal(configBytes, &config)
}
