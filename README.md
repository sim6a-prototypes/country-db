# Simple country database

This project is a prototype of a modular golang application.

## Scripts reference

See scripts details in [scripts reference](./scripts/README.md).

## Quick start

Step 1. Clone the repository `git clone https://github.com/ilya2049/country-db`.

Step 2. Enter to the project root folder `cd country-db`.

Step 3. Install development tools `./scripts/install-dev-tools.sh`. 

Step 4. Create a new configuration `./scripts/configuration.sh new`.

Step 5. Register your application at `https://rapidapi.com` and get your api key. Add received api key in application config (rapidApi.key).

Step 6. Run the application in docker `make run-in-docker`.